All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog]
and this project adheres to [Semantic Versioning].

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

[Unreleased]
============

Nothing.

[0.3.0] - 2018-07-17
==================

Added
-----

  - New `ArtefactQuery` type that's like `ArtefactToken`
    but stores `&str` instead of `String`.
    This requires less allocation than `ArtefactToken`,
    and allows better documentation than a bare tuple.

Changed
-------

  - Made `ValidationError` a non-exhaustive enum,
    so users will be encouraged to add a default case to their matches
    and we can add more detected errors later
    with less compatibility risk.
  - `Channel::validate()` now complains when
    a package is renamed to the same name.
  - `Channel::validate()` now complains when
    a package is renamed,
    but a package by the old name still exists.
  - `Channel::validate()` now complains when
    a package's components refer
    (directly or indirectly)
    back to itself.
  - Everything that took `pkg` and `target` parameters,
    or had `pkg` and `target` fields
    (besides `ArtefactToken`)
    now takes or has an `ArtefactQuery` instead.

[0.2.0] - 2018-06-26
====================

Added
-----

  - Started keeping a change log.

Changed
-------

  - `Channel::lookup_artefact()` switched
    from taking an `ArtefactToken`
    to taking package and target names directly.
    While the former scheme was useful
    for dereferencing `ArtefactTokens` found in other parts of the Channel,
    it was clunky for finding an initial artefact,
    since you had to make your package and target into `String`s
    even if they were `&'static str`.


[0.1.0] - 2018-04-02
====================

Initial release.

[Unreleased]: https://gitlab.com/Screwtapello/rust_release_channel/compare/v0.3.0...master
[0.3.0]: https://gitlab.com/Screwtapello/rust_release_channel/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/Screwtapello/rust_release_channel/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/Screwtapello/rust_release_channel/tree/v0.1.0
