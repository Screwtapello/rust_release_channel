extern crate rust_release_channel;
extern crate toml;

use std::env;
use std::fs;
use std::io;

use std::io::Read;
use std::io::Write;

fn main() {
    for raw_path in env::args_os().skip(1) {
        let mut file = fs::File::open(&raw_path).expect("Could not open file");

        let mut input = String::new();
        file.read_to_string(&mut input)
            .expect("could not read file");

        let parsed: rust_release_channel::Channel =
            input.parse().expect("could not parse");

        let flattened = parsed.to_string().expect("could not serialize");

        io::stdout()
            .write(flattened.as_bytes())
            .expect("could not write result");
    }
}
