rust_release_channel: a data structure for Rust release channel metadata
========================================================================

Repository: <https://gitlab.com/Screwtapello/rust_release_channel/>

Documentation: <https://docs.rs/rust_release_channel>

TODO
----

  - Store hashes as bytes rather than hex-encoded bytes?
      - might make it easier to compare with the results of other hash functions
  - make `impl Display for Channel` format as TOML, instead of the current
    human-readable output? That way we can replace our custom `.to_string()`
    method with the actual `ToString` trait, which might be less surprising.
