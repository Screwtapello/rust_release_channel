#![doc(html_root_url = "https://docs.rs/rust_release_channel/0.3.0")]
#![warn(missing_docs)]
//! A data structure for Rust release channel metadata
//!
//! Introduction
//! ============
//!
//! New versions of the Rust toolchain are released
//! on a regular six-week cycle.
//! Each version is described in a release-channel metadata file
//! in a known location,
//! allowing automated tools to detect new releases
//! and find the tools and components for each supported platform.
//!
//! The data structures in this library
//! represent the information present in each metadata file,
//! organised to help you pull out the specific information you need.
//!
//! This library is designed to handle version 2 of the manifest file format.
//!
//! The most important class is [`Channel`],
//! so you'll want to start there.
//!
//! [`Channel`]: struct.Channel.html
//!
//! First Example
//! =============
//!
//!     extern crate rust_release_channel;
//!     use std::error::Error;
//!
//!     fn dump_manifest_info(input: &str) -> Result<(), Box<Error>> {
//!         // Parse the manifest into a data structure.
//!         let channel: rust_release_channel::Channel = input.parse()?;
//!
//!         // Check the manifest is sensible before we use it.
//!         let errors = channel.validate();
//!         if errors.is_empty() {
//!             // Dump a summary of the manifest data
//!             println!(
//!                 "Channel manifest created on {}",
//!                 channel.date,
//!             );
//!             println!("Included packages:");
//!             for (name, pkg) in channel.pkg.iter() {
//!                 println!("  - {} version {}", name, pkg.version);
//!             }
//!         } else {
//!             println!("Channel has problems:");
//!             for each in errors {
//!                 println!("  - {}", each);
//!             }
//!         }
//!
//!         Ok(())
//!     }
//!
//! Capabilities
//! ============
//!
//! Reading manifests
//! -----------------
//!
//! If you can read the content of an existing manifest file,
//! you can turn it into a queryable, explorable data structure
//!  with the `.parse()` method
//! (courtesy of the standard [`FromStr`] trait).
//!
//! [`FromStr`]: https://doc.rust-lang.org/std/str/trait.FromStr.html
//!
//!     # extern crate rust_release_channel;
//!     # use std::error::Error;
//!     # fn example1() -> Result<(), Box<Error>> {
//!     # let my_str = r#"metadata-version = "2"\ndate = "2018-02-26"\n"#;
//!     use rust_release_channel::Channel;
//!
//!     let channel: Channel = my_str.parse()?;
//!     # Ok(())
//!     # }
//!
//! After reading a manifest,
//! you should call [`.validate()`]
//! to see if the data makes sense before trusting it.
//!
//! [`.validate()`]: struct.Channel.html#method.validate
//!
//! Querying manifests
//! ------------------
//!
//! All the content of the manifest is available to inspect,
//! as native-feeling Rust data structures.
//! For example,
//! where the native manifest file-format models
//! different available file-formats as
//! differently-named keys with the same meaning
//! (`url` vs. `xz_url`, `hash` vs. `xz_hash`),
//! `rust_release_channel` gives you a mapping from
//! [`ArchiveFormat`] to [`ArchiveSource`].
//!
//! [`ArchiveFormat`]: enum.ArchiveFormat.html
//! [`ArchiveSource`]: struct.ArchiveSource.html
//!
//!     # extern crate rust_release_channel;
//!     # use std::error::Error;
//!     # fn example2() -> Result<(), Box<Error>> {
//!     # let my_str = r#"metadata-version = "2"\ndate = "2018-02-26"\n"#;
//!     # let channel: rust_release_channel::Channel = my_str.parse()?;
//!     use rust_release_channel::{ArtefactQuery, ArchiveFormat};
//!
//!     let rust_for_aarch64_url = channel.lookup_artefact(
//!             ArtefactQuery::new("rust", "aarch64-unknown-linux-gnu"),
//!         )
//!         .and_then(|artefact| {
//!              artefact.standalone.get(&ArchiveFormat::TarGzip)
//!         })
//!         .map(|archive| { &archive.url });
//!     # Ok(())
//!     # }
//!
//! Creating fresh manifests
//! ------------------------
//!
//! You can also create manifest data completely from scratch,
//! in case you need to create test-data for another system.
//!
//!     # extern crate rust_release_channel;
//!     # extern crate chrono;
//!     # use std::error::Error;
//!     # fn example3() -> Result<(), Box<Error>> {
//!     use rust_release_channel::{
//!         Channel,
//!         Package,
//!         Artefact,
//!         ArchiveFormat,
//!         ArchiveSource,
//!     };
//!
//!     let source = ArchiveSource::new(
//!         "https://example.com/rust/mypackage-linux-x86.tar.gz".parse()?,
//!         "aa0a89d80329fec6f9e84b79c1674c5427034408630c35da1be442c7da6d2364"
//!             .into(),
//!     );
//!
//!     let mut artefact = Artefact::new();
//!     artefact.standalone.insert(ArchiveFormat::TarGzip, source);
//!
//!     let mut mypackage = Package::new("1.2.3 (abc1234 2018-02-26)".into());
//!     mypackage.target.insert("x86_64-unknown-linux-gnu".into(), artefact);
//!
//!     let mut channel = Channel::new();
//!     channel.pkg.insert("mypackage".into(), mypackage);
//!     # Ok(())
//!     # }
//!
//! Writing manifests
//! -----------------
//!
//! You can turn manifest data back into a string with the [`.to_string()`] method.
//! If you serialize a [`Channel`] and deserialize it again,
//! the result should be identical to the original.
//!
//!     # extern crate rust_release_channel;
//!     # use std::error::Error;
//!     # fn example3() -> Result<(), Box<Error>> {
//!     # let channel = rust_release_channel::Channel::new();
//!     let manifest_string = channel.to_string()?;
//!     # Ok(())
//!     # }
//!
//! [`.to_string()`]: struct.Channel.html#method.to_string
//!
//! Before writing a manifest,
//! you should call [`.validate()`]
//! to check you haven't left the metadata in an inconsistent state.
//!
//! [`.validate()`]: struct.Channel.html#method.validate
extern crate chrono;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate toml;
extern crate url;
extern crate url_serde;

#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;

use std::collections;
use std::error;
use std::fmt;
use std::str::FromStr;

/// The metadata for a Rust release channel.
///
/// This data structure represents
/// all the information available in a Rust release channel metadata file.
/// You can create one from scratch with `new()`,
/// or by parsing an official metadata file:
///
///     # extern crate rust_release_channel;
///     # use std::error::Error;
///     # fn foo() -> Result<(), Box<Error>> {
///     use std::fs;
///     use std::io::Read;
///
///     let mut handle = fs::File::open("examples/channel-rust-1.24.toml")?;
///     let mut buffer = String::new();
///     handle.read_to_string(&mut buffer);
///     let channel: rust_release_channel::Channel = buffer.parse()?;
///     # Ok(())
///     # }
///
/// Look at the [`pkg`] field to find out what packages
/// are available from this channel.
/// If you have something specific in mind,
/// consult the [`.lookup_artefact()`] method.
///
/// [`pkg`]: #structfield.pkg
/// [`.lookup_artefact()`]: #method.lookup_artefact
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Channel {
    /// The creation date of the original metadata file, in `YYYY-MM-DD` format.
    pub date: chrono::NaiveDate,
    /// The packages included in this channel.
    ///
    /// Each key is the name of a package,
    /// the corresponding value is
    /// the metadata for that package.
    pub pkg: collections::BTreeMap<String, Package>,
    /// A mapping from old package names to new ones.
    ///
    /// Sometimes packages get renamed.
    /// For the benefit of systems that have the old name recorded,
    /// this mapping allows them to look up the new name.
    /// Packages that have not been renamed should not appear in this map,
    /// and packages renamed multiple times
    /// should have each legacy name mapped to the current name.
    pub renames: collections::BTreeMap<String, String>,
}

impl Channel {
    /// Create a new, empty Channel whose creation date is today.
    ///
    /// "today" means the current date in the local timezone.
    ///
    ///     # extern crate rust_release_channel;
    ///     # use std::error::Error;
    ///     # fn foo() -> Result<(), Box<Error>> {
    ///     let channel = rust_release_channel::Channel::new();
    ///     # Ok(())
    ///     # }
    pub fn new() -> Channel {
        Channel {
            date: chrono::Local::today().naive_local(),
            pkg: collections::BTreeMap::new(),
            renames: collections::BTreeMap::new(),
        }
    }

    /// Create a new, empty Channel whose creation date is set to the given value.
    ///
    ///     # extern crate rust_release_channel;
    ///     # extern crate chrono;
    ///     # fn foo() {
    ///     let channel = rust_release_channel::Channel::with_date(
    ///         chrono::NaiveDate::from_ymd(2018, 2, 26),
    ///     );
    ///     # }
    pub fn with_date(date: chrono::NaiveDate) -> Channel {
        Channel {
            date: date,
            pkg: collections::BTreeMap::new(),
            renames: collections::BTreeMap::new(),
        }
    }

    /// Serialize this Channel to the standard metadata format.
    ///
    /// If you write the resulting `String` to a file,
    /// it will be a standard channel metadata file.
    ///
    ///     # extern crate rust_release_channel;
    ///     # use std::error::Error;
    ///     # fn foo() -> Result<(), Box<Error>> {
    ///     # let channel = rust_release_channel::Channel::new();
    ///     let metadata_string = channel.to_string()?;
    ///     # Ok(())
    ///     # }
    pub fn to_string(&self) -> Result<String, toml::ser::Error> {
        toml::to_string(self)
    }

    /// Find the [`Artefact`] for an [`ArtefactToken`] or [`ArtefactQuery`].
    ///
    /// If you found an `ArtefactToken` inside
    /// some other `Artefact`'s `components` or `extensions` fields,
    /// you can pass it here to find out what artefact it was talking about.
    ///
    /// If you're interested in some specific `Artefact`
    /// that might or might not be in this channel,
    /// you can create an `ArtefactQuery` and pass it here
    /// to find it if it exists.
    ///
    /// If no artefact exists in this channel for the given package and target,
    /// this method will consult the `renames` map
    /// to see if the requested package is available under a new name.
    /// Note that the rename list is only checked once;
    /// this method will not follow a chain of renames,
    /// so the rename list should map
    /// each historical package name
    /// directly to the latest name for that package.
    ///
    /// If there's still no matching artefact,
    /// this method returns `None`.
    ///
    /// [`Artefact`]: struct.Artefact.html
    /// [`ArtefactToken`]: struct.ArtefactToken.html
    /// [`ArtefactQuery`]: struct.ArtefactQuery.html
    ///
    ///     # extern crate rust_release_channel;
    ///     # let channel = rust_release_channel::Channel::new();
    ///     let query = rust_release_channel::ArtefactQuery::new(
    ///         "rustc",
    ///         "x86_64-unknown-linux-gnu",
    ///     );
    ///     channel.lookup_artefact(query)
    ///         .map(|artefact| {
    ///             println!("Rust for Linux available in these formats:");
    ///             for each in artefact.standalone.keys() {
    ///                 println!("- {}", each);
    ///             }
    ///         });
    pub fn lookup_artefact<'s, 'q, T>(
        &'s self,
        query: T,
    ) -> Option<&'s Artefact>
    where
        T: Into<ArtefactQuery<'q>>,
    {
        let query: ArtefactQuery = query.into();

        // Find the package containing this artefact
        self.pkg
            .get(query.pkg)
            .or_else(|| {
                // No package by that name exists; perhaps it was renamed?
                self.renames.get(query.pkg).and_then(|new_name| {
                    // Yes, it was reramed!
                    // Find the package under its new name.
                    self.pkg.get(new_name)
                })
            })
            .and_then(|pkg| {
                // Find the target containing this artefact.
                pkg.target.get(query.target)
            })
    }

    /// Validate the internal consistency of this metadata.
    ///
    /// Specifically:
    ///
    ///   - If [`renames`] says a package named X was renamed to Y,
    ///     then Y should exist in the [`pkg`] map
    ///     and X should not.
    ///   - If a package is renamed,
    ///     the new name must be different to the old name.
    ///   - Each component or extension in an [`Artefact`] should reference
    ///     the name of a package in the channel's [`pkg`] map,
    ///     and a target in that package's [`target`] map.
    ///   - The "components" links between artefacts must be acyclic;
    ///     that is,
    ///     an artefact must not include itself as a component,
    ///     or include some other artefact that
    ///     (directly or indirectly)
    ///     includes it as a component.
    ///
    /// Returns all detected problems.
    ///
    ///     # extern crate rust_release_channel;
    ///     # fn foo() {
    ///     # let channel = rust_release_channel::Channel::new();
    ///     let errors = channel.validate();
    ///     if !errors.is_empty() {
    ///         println!("Errors found:");
    ///         for each in errors {
    ///             println!("  - {}", each);
    ///         }
    ///     }
    ///     # }
    ///
    /// [`renames`]: #structfield.renames
    /// [`pkg`]: #structfield.pkg
    /// [`Artefact`]: struct.Artefact.html
    /// [`target`]: struct.Artefact.html#structfield.target
    pub fn validate<'a>(&'a self) -> Vec<ValidationError<'a>> {
        let pkg_problems = self.pkg
            .iter()
            .flat_map(|(name, pkg)| pkg.validate(self, name));

        let rename_problems = self.renames.iter().filter_map(|(old, new)| {
            if old == new {
                Some(ValidationError::RenameToItself(new))
            } else if self.pkg.contains_key(old) {
                Some(ValidationError::RenameExistingPackage(old))
            } else if !self.pkg.contains_key(new) {
                Some(ValidationError::RenameToUnknownPackage { old, new })
            } else {
                None
            }
        });

        let mut cycle_problems = collections::BTreeSet::new();
        for (pkg_name, pkg) in &self.pkg {
            for target_name in pkg.target.keys() {
                for cycle in validate_depenency_cycles(
                    self,
                    ArtefactQuery::new(pkg_name, target_name),
                ) {
                    cycle_problems.insert(cycle);
                }
            }
        }
        let cycle_problems = cycle_problems
            .into_iter()
            .map(ValidationError::DependencyLoop);

        pkg_problems
            .chain(rename_problems)
            .chain(cycle_problems)
            .collect()
    }
}

impl Default for Channel {
    fn default() -> Self {
        Channel::new()
    }
}

impl FromStr for Channel {
    type Err = toml::de::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        toml::from_str(s)
    }
}

impl fmt::Display for Channel {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "Rust release channel for version {}, dated {}",
            self.pkg
                .get("rust")
                .map(|pkg| pkg.version.as_str())
                .unwrap_or("<unknown>"),
            self.date,
        )
    }
}

/// A single package installable from this release channel.
///
/// Building a package for a particular target
/// produces an artefact,
/// but every artefact should be built from the same source.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, PartialOrd,
         Ord, Hash)]
pub struct Package {
    /// A human-readable version-number for this package.
    ///
    /// This should be a [SemVer] version number of the form `X.Y.Z`,
    /// followed by a space,
    /// an open-parenthesis,
    /// a Git commit ID truncated to 9 digits,
    /// a space,
    /// the date of that commit in `YYYY-MM-DD` format,
    /// and a close-parenthesis:
    ///
    /// ```text
    /// 1.2.3 (a1b2c3d4e 2018-02-26)
    /// ```
    ///
    /// [SemVer]: https://semver.org/
    pub version: String,
    /// The complete Git commit ID of this version of this package.
    ///
    /// May not be present if the commit ID is not known,
    /// or this package's source is not stored in a Git repository.
    pub git_commit_hash: Option<String>,
    /// A mapping from target names to [`Artefact`]s of this package built
    /// for that target.
    ///
    /// A target name
    /// (sometimes "[target triplet][tt]",
    /// although there's not always three parts)
    /// is a name that identifies a particular hardware and software platform,
    /// such as `x86_64-unknown-linux-gnu` (for modern Linux PCs)
    /// or `i686-pc-windows-msvc`
    /// (for 32-bit Windows with the Visual C++ compiler).
    /// Software built for a particular target
    /// usually won't work on a different target,
    /// so if you're looking for a particular package
    /// you'll want to get the artefact for the platform you're using.
    ///
    /// The special target name `*` (an asterisk)
    /// is used for packages that are the same on every concievable platform,
    /// like `rust-src` which contains the Rust source code.
    ///
    /// [tt]: https://wiki.osdev.org/Target_Triplet
    /// [`Artefact`]: struct.Artefact.html
    #[serde(skip_serializing_if = "::std::collections::BTreeMap::is_empty")]
    pub target: collections::BTreeMap<String, Artefact>,
}

impl Package {
    /// Create a new, empty Package whose version is set to the given value.
    ///
    /// By convention, `version` is in the format `X.Y.Z (HHHHHHH YYYY-MM-DD)`
    /// where `X.Y.Z` is a SemVer version string,
    /// `HHHHHHH` is the abbreviated commit ID of the source repository
    /// at that version,
    /// and `YYYY-MM-DD` is the date when that version was released.
    ///
    ///     # extern crate rust_release_channel;
    ///     let pkg = rust_release_channel::Package::new(
    ///         "1.2.3 (abc1234 2018-02-26)".into()
    ///     );
    ///
    pub fn new(version: String) -> Package {
        Package {
            version: version,
            git_commit_hash: None,
            target: collections::BTreeMap::new(),
        }
    }

    fn validate<'a>(
        &'a self,
        channel: &'a Channel,
        pkg: &'a str,
    ) -> Vec<ValidationError<'a>> {
        self.target
            .iter()
            .flat_map(|(target, artefact)| {
                artefact.validate(channel, pkg, target)
            })
            .collect()
    }
}

impl fmt::Display for Package {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "Package version {} for {} target(s)",
            self.version,
            self.target.len(),
        )
    }
}

/// One or more packages built for a single target.
///
/// All the relevant files are grouped together into an archive.
/// The `standalone` field may reference
/// multiple archives in different formats,
/// but each archive should contain exactly the same files.
///
/// If this artefact is a combination of other, smaller artefacts,
/// they are mentioned in the `components` field.
/// Installing all the components
/// should be equivalent to
/// installing the standalone archive.
/// The `extensions` field suggests other artefacts
/// that might be useful, but are not part of this artefact.
///
/// All the fields in this structure may be empty,
/// which can occur when this package was not built for this target
/// for some reason.
#[derive(Default, Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Artefact {
    /// Points to archives containing the files of this artefact.
    ///
    /// Each archive contains exactly the same files,
    /// so you can use whichever format is most convenient.
    pub standalone: collections::BTreeMap<ArchiveFormat, ArchiveSource>,
    /// Points to the individual artefacts
    /// that make up the `standalone` artefact, if any.
    ///
    /// If you don't need some particular artefact
    /// that's included by default,
    /// you can save bandwidth by just downloading
    /// the artefacts you need.
    pub components: Vec<ArtefactToken>,
    /// Points to other artefacts that may be useful with this one.
    ///
    /// In particular, this artefact may be able to use
    /// certain other artefacts built for different targets,
    /// and this field describes which ones.
    pub extensions: Vec<ArtefactToken>,
}

impl Artefact {
    /// Create a new, empty Artefact.
    ///
    ///     # extern crate rust_release_channel;
    ///     let pkg = rust_release_channel::Artefact::new();
    ///
    pub fn new() -> Artefact {
        Default::default()
    }

    fn validate<'a>(
        &'a self,
        channel: &'a Channel,
        pkg: &'a str,
        target: &'a str,
    ) -> Vec<ValidationError<'a>> {
        let mut res = Vec::new();

        for token in self.components.iter() {
            let component_pkg = channel.pkg.get(&token.pkg);
            let component_target = component_pkg
                .clone()
                .and_then(|pkg| pkg.target.get(&token.target));

            match (component_pkg, component_target) {
                (None, _) => {
                    res.push(ValidationError::ArtefactHasUnknownComponent {
                        artefact: ArtefactQuery::new(pkg, target),
                        component_pkg: &token.pkg,
                    })
                }
                (Some(_), None) => res.push(
                    ValidationError::ArtefactComponentHasUnknownTarget {
                        artefact: ArtefactQuery::new(pkg, target),
                        component: ArtefactQuery::new(
                            &token.pkg,
                            &token.target,
                        ),
                    },
                ),
                _ => (),
            }
        }

        for token in self.extensions.iter() {
            let extension_pkg = channel.pkg.get(&token.pkg);
            let extension_target = extension_pkg
                .clone()
                .and_then(|pkg| pkg.target.get(&token.target));

            match (extension_pkg, extension_target) {
                (None, _) => {
                    res.push(ValidationError::ArtefactHasUnknownExtension {
                        artefact: ArtefactQuery::new(pkg, target),
                        extension_pkg: &token.pkg,
                    })
                }
                (Some(_), None) => res.push(
                    ValidationError::ArtefactExtensionHasUnknownTarget {
                        artefact: ArtefactQuery::new(pkg, target),
                        extension: ArtefactQuery::new(
                            &token.pkg,
                            &token.target,
                        ),
                    },
                ),
                _ => (),
            }
        }

        res
    }
}

impl fmt::Display for Artefact {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "Artefact in {} format(s), with {} component(s) and {} extension(s)",
            self.standalone.len(),
            self.components.len(),
            self.extensions.len(),
        )
    }
}

fn sort_cycle<T>(input: &[T]) -> Vec<T>
where
    T: Ord + Clone,
{
    // If the input is empty, it's implictly sorted, and we have nothing to do.
    let len = input.len();
    if len == 0 {
        return Vec::new();
    }

    // There's at least one item, so we want to find the smallest one.
    let index_min = input
        .iter()
        .enumerate()
        // put the item before the index, so .min() will find the minimum item,
        // not the minimum index.
        .map(|(index, item)| (item, index))
        .min()
        // We only want the index, we don't care about the item itself.
        .map(|(_item, index)| index)
        .expect("We already handled the zero-item case");

    // Move all the items from input into a new vector, with the smallest at the
    // beginning, but maintaining their relative order.
    let mut res = Vec::with_capacity(len);
    res.extend_from_slice(&input[index_min..]);
    res.extend_from_slice(&input[0..index_min]);

    res
}

fn validate_depenency_cycles<'a>(
    channel: &'a Channel,
    query: ArtefactQuery<'a>,
) -> collections::BTreeSet<Vec<ArtefactQuery<'a>>> {
    let mut to_check = vec![(Vec::new(), query)];
    let mut res = collections::BTreeSet::new();

    while let Some((ancestors, query)) = to_check.pop() {
        let cycle_start =
            ancestors.iter().position(|ancestor| ancestor == &query);

        match cycle_start {
            Some(index) => {
                res.insert(sort_cycle(&ancestors[index..]));
            }
            None => {
                channel.lookup_artefact(query).map(|artefact| {
                    for each in &artefact.components {
                        let mut dep_path = ancestors.clone();
                        dep_path.push(query);

                        to_check.push((dep_path, each.into()));
                    }
                });
            }
        };
    }

    res
}

/// Describes where to obtain a particular archive.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ArchiveSource {
    /// A URL from where the archive may be obtained.
    pub url: url::Url,
    /// The expected SHA256 hash of the archive,
    /// encoded as ASCII lowercase hexadecimal digits.
    ///
    /// If you download this archive,
    /// hash it and compare the result to this value,
    /// so you can tell whether you got the right file.
    pub hash: String,
}

impl ArchiveSource {
    /// Create a new ArchiveSource for the given URL and hash.
    ///
    /// `url` is a URL from where the archive may be obtained.
    ///
    /// `hash` is the expected SHA256 hash of the archive,
    /// encoded as ASCII lowercase hexadecimal digits.
    ///
    ///     # extern crate rust_release_channel;
    ///     # use std::error::Error;
    ///     # fn example() -> Result<(), Box<Error>> {
    ///     let pkg = rust_release_channel::ArchiveSource::new(
    ///         "https://example.com/rust/archive.tar.gz".parse()?,
    ///         "867f3496607caeb969dabcd0083d35e010591bc2a9105af4f6c34289750c8b95"
    ///             .into(),
    ///     );
    ///     # Ok(())
    ///     # }
    ///
    pub fn new(url: url::Url, hash: String) -> ArchiveSource {
        ArchiveSource { url, hash }
    }
}

impl fmt::Display for ArchiveSource {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "<{}>", self.url)
    }
}

/// Represents the formats used by archives from this channel.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ArchiveFormat {
    /// The archive is a tar archive compressed with [gzip].
    ///
    /// [gzip]: http://www.gzip.org/
    TarGzip,
    /// The archive is a tar archive compressed with [xz].
    ///
    /// [xz]: https://tukaani.org/xz/
    TarXz,
    // Discourage people from exhaustively matching this enum
    // so we can add more formats in future.
    #[doc(hidden)]
    ReservedForFutureExpansion,
}

impl fmt::Display for ArchiveFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            &ArchiveFormat::TarGzip => write!(f, ".tar.gz"),
            &ArchiveFormat::TarXz => write!(f, ".tar.xz"),
            _ => write!(f, "<unknown>"),
        }
    }
}

/// Stores the identity of an [`Artefact`] in a channel.
///
/// This allows one artefact in a channel to indicate which other artefacts
/// it works with.
///
///     # extern crate rust_release_channel;
///     # let mut artefact = rust_release_channel::Artefact::new();
///     let token = rust_release_channel::ArtefactToken::new(
///         "rls".into(),
///         "x86_64-unknown-linux-gnu".into(),
///     );
///
///     artefact.extensions.push(token);
///
/// If you want to name an artefact
/// that may or may not exist
/// in a channel,
/// you might want [`ArtefactQuery`] instead.
///
/// Note that `ArtefactQuery` implements [`From<&ArtefactToken>`],
/// so if you have an `ArtefactToken`
/// you can cheaply obtain an `ArtefactQuery` whenever you want
/// with `.into()`:
///
///     # extern crate rust_release_channel;
///     # let channel = rust_release_channel::Channel::new();
///     # fn func_that_takes_query(_: rust_release_channel::ArtefactQuery) {};
///     # let token = rust_release_channel::ArtefactToken::new(
///     #   "rls".into(),
///     #   "x86_64-unknown-linux-gnu".into(),
///     # );
///     func_that_takes_query((&token).into());
///
/// [`From<&ArtefactToken>`]: https://doc.rust-lang.org/stable/std/convert/trait.From.html
/// [`Artefact`]: struct.Artefact.html
/// [`ArtefactQuery`]: struct.ArtefactQuery.html
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, PartialOrd,
         Ord, Hash)]
pub struct ArtefactToken {
    /// The name of the package that was built to produce the target artefact.
    ///
    /// See [`Channel::pkg`] for details.
    ///
    /// [`Channel::pkg`]: struct.Channel.html#structfield.pkg
    pub pkg: String,
    /// The name of the target the package was built for to produce the target
    /// artefact.
    ///
    /// See [`Package::target`] for details.
    ///
    /// [`Package::target`]: struct.Package.html#structfield.target
    pub target: String,
}

impl ArtefactToken {
    /// Create a new ArtefactToken for the given package and target.
    ///
    /// `pkg` is the name of the package that was built
    /// to produce the target artefact.
    ///
    /// `target` is the name of the target the package was built for
    /// to produce the target artefact.
    ///
    ///     # extern crate rust_release_channel;
    ///     let token = rust_release_channel::ArtefactToken::new(
    ///         "somepackage".into(),
    ///         "aarch64-unknown-linux-gnu".into(),
    ///     );
    ///
    /// [`pkg`]: struct.Channel.html#structfield.pkg
    pub fn new(pkg: String, target: String) -> ArtefactToken {
        ArtefactToken { pkg, target }
    }
}

impl fmt::Display for ArtefactToken {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "Package {} built for {}", self.pkg, self.target,)
    }
}

impl<'a> PartialEq<ArtefactQuery<'a>> for ArtefactToken {
    fn eq(&self, other: &ArtefactQuery<'a>) -> bool {
        self.pkg == other.pkg && self.target == other.target
    }
}

/// Names an [`Artefact`] that might exist in a channel.
///
/// This structure provides
/// a way to talk about and refer to artefacts
/// without having to find an already-existing [`ArtefactToken`],
/// or construct a whole new one.
///
///     # extern crate rust_release_channel;
///     # let channel = rust_release_channel::Channel::new();
///     let query = rust_release_channel::ArtefactQuery::new(
///         "rust",
///         "x86_64-unknown-linux-gnu",
///     );
///     let artefact = channel.lookup_artefact(query);
///
/// [`Artefact`]: struct.Artefact.html
/// [`ArtefactToken`]: struct.ArtefactToken.html
#[derive(Deserialize, Serialize, Debug, Copy, Clone, PartialEq, Eq,
         PartialOrd, Ord, Hash)]
pub struct ArtefactQuery<'a> {
    /// The name of the package that was built to produce the target artefact.
    ///
    /// See [`Channel::pkg`] for details.
    ///
    /// [`Channel::pkg`]: struct.Channel.html#structfield.pkg
    pub pkg: &'a str,
    /// The name of the target the package was built for to produce the target
    /// artefact.
    ///
    /// See [`Package::target`] for details.
    ///
    /// [`Package::target`]: struct.Package.html#structfield.target
    pub target: &'a str,
}

impl<'a> ArtefactQuery<'a> {
    /// Create a new ArtefactQuery for the given package and target.
    ///
    /// `pkg` is the name of the package that was built
    /// to produce the target artefact.
    ///
    /// `target` is the name of the target the package was built for
    /// to produce the target artefact.
    ///
    ///     # extern crate rust_release_channel;
    ///     let query = rust_release_channel::ArtefactQuery::new(
    ///         "somepackage",
    ///         "aarch64-unknown-linux-gnu",
    ///     );
    ///
    pub fn new(pkg: &'a str, target: &'a str) -> ArtefactQuery<'a> {
        ArtefactQuery { pkg, target }
    }

    /// Create an [`ArtefactToken`] with the same contents.
    ///
    /// This allocates new copies of the `pkg` and `target` fields
    /// so that the new `ArtefactToken` struct can own them.
    ///
    /// This is effectively the same thing as:
    ///
    ///     # extern crate rust_release_channel;
    ///     # let query = rust_release_channel::ArtefactQuery::new("a", "b");
    ///     let token = rust_release_channel::ArtefactToken::new(
    ///         query.pkg.into(),
    ///         query.target.into(),
    ///     );
    ///
    /// An example:
    ///
    ///     # extern crate rust_release_channel;
    ///     # let mut artefact = rust_release_channel::Artefact::new();
    ///     let token = rust_release_channel::ArtefactQuery::new(
    ///         "rls",
    ///         "x86_64-unknown-linux-gnu",
    ///     ).to_token();
    ///
    ///     artefact.extensions.push(token);
    ///
    /// [`ArtefactToken`]: struct.ArtefactToken.html
    pub fn to_token(&self) -> ArtefactToken {
        ArtefactToken {
            pkg: self.pkg.into(),
            target: self.target.into(),
        }
    }
}

impl<'a> fmt::Display for ArtefactQuery<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "Package {} built for {}", self.pkg, self.target,)
    }
}

impl<'a> PartialEq<ArtefactToken> for ArtefactQuery<'a> {
    fn eq(&self, other: &ArtefactToken) -> bool {
        self.pkg == other.pkg && self.target == other.target
    }
}

impl<'a> From<&'a ArtefactToken> for ArtefactQuery<'a> {
    fn from(src: &'a ArtefactToken) -> ArtefactQuery<'a> {
        ArtefactQuery::new(&src.pkg, &src.target)
    }
}

/// Possible ways in which channel metadata could be inconsistent.
///
/// See [`Channel::validate`] for more information.
///
/// [`Channel::validate`]: struct.Channel.html#method.validate
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, PartialOrd,
         Ord, Hash)]
pub enum ValidationError<'a> {
    /// The channel's [`renames`] field
    /// says package `old` was renamed to `new`,
    /// but no package named `new` is listed in the channel's [`pkg`] map.
    ///
    /// [`renames`]: struct.Channel.html#structfield.renames
    /// [`pkg`]: struct.Channel.html#structfield.pkg
    RenameToUnknownPackage {
        /// The old name for a package.
        old: &'a str,
        /// The alleged new name for the package,
        /// which isn't listed in the channel metadata.
        new: &'a str,
    },
    /// The channel's [`renames`] field
    /// says this package was renamed to the same name.
    ///
    /// [`renames`]: struct.Channel.html#structfield.renames
    RenameToItself(&'a str),
    /// The channel's [`renames`] field
    /// says this package was renamed,
    /// but it's still listed in the channel's [`pkg`] map.
    ///
    /// [`renames`]: struct.Channel.html#structfield.renames
    /// [`pkg`]: struct.Channel.html#structfield.pkg
    RenameExistingPackage(&'a str),
    /// `artefact` claims one of its [`components`]
    /// is an artefact built from a package named `component_pkg`,
    /// but no such package is listed in the channel's [`pkg`] map.
    ///
    /// [`components`]: struct.Artefact.html#structfield.components
    /// [`pkg`]: struct.Channel.html#structfield.pkg
    ArtefactHasUnknownComponent {
        /// The artefact that claims an unknown component.
        artefact: ArtefactQuery<'a>,
        /// The alleged package name
        /// that is a component of `artefact`.
        component_pkg: &'a str,
    },
    /// `artefact` claims
    /// one of its [`components`] is `component`,
    /// but `component.target` is not listed
    /// in `component.pkg`'s [`target`] map.
    ///
    /// [`components`]: struct.Artefact.html#structfield.components
    /// [`target`]: struct.Package.html#structfield.target
    ArtefactComponentHasUnknownTarget {
        /// The artefact that claims a component
        /// with an unknown target.
        artefact: ArtefactQuery<'a>,
        /// The alleged artefact with the unknown target.
        component: ArtefactQuery<'a>,
    },
    /// `artefact` claims one of its [`extensions`]
    /// is an artefact built from a package named `extension_pkg`,
    /// but no such package is listed in the channel's [`pkg`] map.
    ///
    /// [`extensions`]: struct.Artefact.html#structfield.extensions
    /// [`pkg`]: struct.Channel.html#structfield.pkg
    ArtefactHasUnknownExtension {
        /// The artefact that claims an unknown extension.
        artefact: ArtefactQuery<'a>,
        /// The alleged package name
        /// that is a extension of this artefact.
        extension_pkg: &'a str,
    },
    /// `artefact` claims
    /// one of its [`extensions`] is `extension`,
    /// but `extension.target` is not listed
    /// in `extension.pkg`'s [`target`] map.
    ///
    /// [`extensions`]: struct.Artefact.html#structfield.extensions
    /// [`target`]: struct.Package.html#structfield.target
    ArtefactExtensionHasUnknownTarget {
        /// The artefact that claims an extension
        /// with an unknown target.
        artefact: ArtefactQuery<'a>,
        /// The alleged artefact with the unknown target.
        extension: ArtefactQuery<'a>,
    },
    /// A dependency loop was detected
    /// through all these artefacts.
    ///
    /// That is,
    /// each artefact lists the next artefact as a required component,
    /// and the last artefact requires the first one.
    DependencyLoop(Vec<ArtefactQuery<'a>>),

    #[doc(hidden)]
    __Nonexhaustive,
}

impl<'a> fmt::Display for ValidationError<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            &ValidationError::RenameToUnknownPackage { old, new } => write!(
                f,
                "Package {:?} renamed to unknown package {:?}",
                old, new,
            ),
            &ValidationError::RenameToItself(pkg) => {
                write!(f, "Package {:?} renamed to the same name", pkg)
            }
            &ValidationError::RenameExistingPackage(pkg) => {
                write!(f, "Package {:?} is renamed, but still exists", pkg)
            }
            &ValidationError::ArtefactHasUnknownComponent {
                artefact,
                component_pkg,
            } => write!(
                f,
                "{:?} has unknown component {:?}",
                artefact, component_pkg,
            ),
            &ValidationError::ArtefactComponentHasUnknownTarget {
                artefact,
                component,
            } => write!(
                f,
                "{:?} has component for unknown target {:?}",
                artefact, component,
            ),
            &ValidationError::ArtefactHasUnknownExtension {
                artefact,
                extension_pkg,
            } => write!(
                f,
                "{:?} has unknown extension {:?}",
                artefact, extension_pkg,
            ),
            &ValidationError::ArtefactExtensionHasUnknownTarget {
                artefact,
                extension,
            } => write!(
                f,
                "{:?} has extension for unknown target {:?}",
                artefact, extension,
            ),
            &ValidationError::DependencyLoop(ref members) => write!(
                f,
                "Dependency loop among packages: {}",
                members
                    .iter()
                    .map(|query| format!("{} for {}", query.pkg, query.target))
                    .collect::<Vec<_>>()[..]
                    .join(", ")
            ),
            &ValidationError::__Nonexhaustive => unreachable!(),
        }
    }
}

impl<'a> error::Error for ValidationError<'a> {
    fn description(&self) -> &str {
        match self {
            &ValidationError::RenameToUnknownPackage { .. } => {
                "A package was renamed to an unknown name"
            }
            &ValidationError::RenameToItself(_) => {
                "A package was renamed to the same name"
            }
            &ValidationError::RenameExistingPackage(_) => {
                "A package was renamed, but still exists under the old name"
            }
            &ValidationError::ArtefactHasUnknownComponent { .. } => {
                "An artefact has an unknown component"
            }
            &ValidationError::ArtefactComponentHasUnknownTarget { .. } => {
                "An artefact has a component with an unknown target"
            }
            &ValidationError::ArtefactHasUnknownExtension { .. } => {
                "An artefact has an unknown extension"
            }
            &ValidationError::ArtefactExtensionHasUnknownTarget { .. } => {
                "An artefact has an extension with an unknown target"
            }
            &ValidationError::DependencyLoop(_) => {
                "A package has a dependency loop."
            }
            &ValidationError::__Nonexhaustive => unreachable!(),
        }
    }
}

mod serialization;

#[cfg(test)]
mod tests;
